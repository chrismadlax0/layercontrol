﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="PNG" Type="Folder">
			<Item Name="computer-chip.png" Type="Document" URL="../computer-chip.png"/>
			<Item Name="desktop-computer.png" Type="Document" URL="../desktop-computer.png"/>
			<Item Name="hardware.png" Type="Document" URL="../hardware.png"/>
		</Item>
		<Item Name="Main.vi" Type="VI" URL="../Main.vi"/>
		<Item Name="Modify.vi" Type="VI" URL="../Modify.vi"/>
		<Item Name="PictureRing.ctl" Type="VI" URL="../PictureRing.ctl"/>
		<Item Name="UsePictureRing.vi" Type="VI" URL="../UsePictureRing.vi"/>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
